﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
    	while (true) {
	    	double zuZahlenderBetrag = fahrkartenbestellungErfassen();
	    	double rueckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
	    	fahrkartenAusgeben();
	    	rueckgeldAusgeben(rueckgabebetrag);
	
	    	System.out.println("\nVergessen Sie nicht, die Fahrscheine\n"+
	                          "vor Fahrtantritt entwerten zu lassen!\n"+
	                          "Wir wünschen Ihnen eine gute Fahrt.\n\n"+
	                          "--------------------------------------------------\n\n");
    	}
    }
    
    public static double fahrkartenbestellungErfassen() {
    	Scanner eingabe = new Scanner(System.in);
    	
    	boolean running = true;
    	double ticketEinzelpreis = 1.0;
    	
        String[] fahrkartenBezeichnung = {"Einzelfahrschein Berlin AB", "Einzelfahrschein Berlin BC", "Einzelfahrschein Berlin ABC",
        								  "Kurzstrecke", "Tageskarte Berlin AB", "Tageskarte Berlin BC", "Tageskarte Berlin ABC",
        								  "Kleingruppen-Tageskarte Berlin AB", "Kleingruppen-Tageskarte Berlin BC",
        								  "Kleingruppen-Tageskarte Berlin ABC"};
    	double[] fahrkartenPreis = {2.9, 3.3, 3.6, 1.9, 8.6, 9.0, 9.6, 23.5, 24.3, 24.9};

    	//Der Vorteil dieser Implementierung ist der, dass es so uebersichtlicher im Code ist und man
    	//einfacher Einträge den Arrays hinzufuegen kann
               
        do {
        	System.out.println("Bitte waehlen Sie Ihr Wunschticket (Zahl waehlen):\n");
        	
        	for (int i = 0; i < fahrkartenPreis.length; i++) {
        		System.out.printf("(%s) %s --> %.2f Euro\n", i, fahrkartenBezeichnung[i], fahrkartenPreis[i]);
        	}
        	
        	System.out.println("Geben Sie -1 ein, um das Programm zu beenden.\n");
        	int auswahl = eingabe.nextInt();
        	if (auswahl == -1) {
        		System.out.println("Bis bald!");
        		System.exit(0);
        	}
        	
        	if (auswahl < 0 || auswahl > fahrkartenPreis.length - 1) {
        		System.out.println("Dieses Ticket existiert nicht!\n");
        	} else {
        		running = false;
        		ticketEinzelpreis = fahrkartenPreis[auswahl];
        	}
        } while (running);
        
        System.out.print("Wie viele Tickets möchten Sie erwerben?: ");
        double anzahlTickets = eingabe.nextInt();
    	
    	return anzahlTickets * ticketEinzelpreis;
    }
    
    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
    	Scanner tastatur = new Scanner(System.in);
    	
    	double eingezahlterGesamtbetrag = 0.0;
        double eingeworfeneMünze;
        
    	while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
     	   //System.out.println("Noch zu zahlen: " + (zuZahlenderBetrag - eingezahlterGesamtbetrag));
     	   System.out.printf("%s%.2f EURO %s", "Noch zu zahlen: ", (zuZahlenderBetrag - eingezahlterGesamtbetrag),"  ");
     	   System.out.print("\nEingabe (mind. 5Ct, höchstens 2 Euro): ");
     	   eingeworfeneMünze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneMünze;
        }
    	
    	return eingezahlterGesamtbetrag - zuZahlenderBetrag;
    }
    
    public static void warte (int time) {
    	try {
  			Thread.sleep(time);
  		} catch (InterruptedException e) {
  			e.printStackTrace();
  		}
    }
    
    public static void fahrkartenAusgeben() {
    	System.out.println("\nFahrscheine werden ausgegeben.");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           warte(250);
        }
        System.out.println("\n\n");
    }
    
    public static void muenzeAusgeben(double rueckgabebetrag, String einheit) {
    	
    	while(rueckgabebetrag >= 2.0) // 2 EURO-Münzen
        {
     	  System.out.println("2 " + einheit);
	          rueckgabebetrag -= 2.0;
        }
        while(rueckgabebetrag >= 1.0) // 1 EURO-Münzen
        {
     	  System.out.println("1 " + einheit);
	          rueckgabebetrag -= 1.0;
        }
        while(rueckgabebetrag >= 0.5) // 50 CENT-Münzen
        {
     	  System.out.println("0.50 " + einheit);
	          rueckgabebetrag -= 0.5;
        }
        while(rueckgabebetrag >= 0.2) // 20 CENT-Münzen
        {
     	  System.out.println("0.20 " + einheit);
	          rueckgabebetrag -= 0.2;
        }
        while(rueckgabebetrag >= 0.1) // 10 CENT-Münzen
        {
     	  System.out.println("0.10 " + einheit);
	          rueckgabebetrag -= 0.1;
        }
        while(rueckgabebetrag >= 0.05)// 5 CENT-Münzen
        {
     	  System.out.println("0.05 " + einheit);
	          rueckgabebetrag -= 0.05;
        }
    }
    
    
    public static void rueckgeldAusgeben(double rueckgabebetrag) {
    	if(rueckgabebetrag > 0.0)
        {
     	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO", rueckgabebetrag);
     	   System.out.println("\nwird in den folgenden Münzen ausgezahlt:");
     	   muenzeAusgeben(rueckgabebetrag, " EURO");
        }
    }
}