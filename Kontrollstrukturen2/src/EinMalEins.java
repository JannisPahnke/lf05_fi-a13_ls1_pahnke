public class EinMalEins {
  
  public static void main(String[] args) {
    
    int x, y;
    
    for(x = 1; x <= 10; x++) {
      
      for(y = 1; y <= 10; y++) {
        
        System.out.printf("%5d", y * x);
      }
      
      System.out.print("\n");
    }
  }
}