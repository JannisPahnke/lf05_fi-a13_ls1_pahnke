import java.util.Scanner;
 
public class bmi {
  
  public static void main(String[] args) {
    
    
    double gewicht; 
    String geschlecht;
    double bmi;    
    double kilogramm;
    double bmiKilogramm;
    
    Scanner werteingabe = new Scanner(System.in); 
    String konsolenAusgabe = "";
    String abfrage;

    
    abfrage="Geben Sie Ihre K�rpergr��e in cm an: ";
    System.out.println("");
    System.out.print(abfrage);                 //Speichern des Wertes f�r die K�rpergr��e
    kilogramm = werteingabe.nextDouble(); 
    abfrage="Geben Sie Ihr Gewicht in kg an:";
    System.out.print(abfrage);
    gewicht = werteingabe.nextDouble();      //Speichern des Wertes f�r das Gewicht
    abfrage="Geben Sie Ihr Geschlecht an:";
    System.out.print(abfrage);
    geschlecht = werteingabe.next(); //Bestimmung des Geschlechts durch Werte "m" oder "w"
    
    
    bmiKilogramm = kilogramm/100;
    bmi = gewicht/(bmiKilogramm*bmiKilogramm);
    
    if (geschlecht.contains("m")){
      if (bmi<20){
        konsolenAusgabe="Sie haben Untergewicht " + bmi;
      }
      else if(bmi>=20 && bmi<=25){
        konsolenAusgabe="Sie haben Normalgewicht " + bmi;
      }
      else{
        konsolenAusgabe="Sie haben �bergewicht " + bmi;
      }
    }
    else if (geschlecht.contains("w")){
      if (bmi<19){
        konsolenAusgabe="Sie haben Untergewicht " + bmi;
      }
      else if(bmi>=19 && bmi<=24){
        konsolenAusgabe="Sie haben Normalgewicht " + bmi;
      }
      else{
        konsolenAusgabe="Sie haben �bergewicht " + bmi;
      }
    }
    else{
      konsolenAusgabe="Das ist kein Geschlecht";
    }
    
    System.out.println("");
    System.out.println(konsolenAusgabe);    
    
    
  }
}
  
