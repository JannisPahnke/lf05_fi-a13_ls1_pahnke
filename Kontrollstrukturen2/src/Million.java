
import java.util.Scanner;

public class Million {

	public static void main(String[] Args) {

		Scanner eingabe = new Scanner(System.in);
		double kapital, zinssatz;
		char wiederholung = 'j';
		int i = 1;

		while (wiederholung == 'j') {

			// Eingabe
			System.out.print("Wie viel Kapital m�chten Sie anlegen? ");
			kapital = eingabe.nextDouble();

			System.out.print("Geben Sie den aktuellen Zinssatz an: ");
			zinssatz = eingabe.nextDouble();
			zinssatz = zinssatz / 100 + 1;
			
			//Rechnung
			while (kapital < 1000000) {
				kapital = kapital * zinssatz;
				i++;
			}
			// Ausgabe
			System.out.println("Nach " + i + " Jahren besitzen Sie 1.000.000 Euro");
			
			System.out.println("M�chten Sie eine weitere Rechnung d�rchf�hren? (j/n)");
			wiederholung = eingabe.next().charAt(0);
		}
		eingabe.close();
	}
}