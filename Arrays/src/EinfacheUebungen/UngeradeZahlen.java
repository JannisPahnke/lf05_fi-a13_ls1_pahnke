package EinfacheUebungen;

public class UngeradeZahlen {

	public static void main(String[] args) {

		int[] zahlen = new int[10];
		
		for(int i = 0; i < zahlen.length; i++) {   //Zahlen von 1 bis 19 
			zahlen[i] = 2 * i + 1;
		}
		
		for(int i = 0; i < zahlen.length; i++) {
			System.out.println(zahlen[i]);
		}
	}
}
