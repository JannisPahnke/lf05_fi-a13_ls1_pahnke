package EinfacheUebungen;
public class Lotto {

	public static void main(String[] args) {
		
		// Aufgabe a)

		int[] lottozahl = new int[6];
		
		lottozahl[0] = 3;       //alle Lottozahlen speichern
		lottozahl[1] = 7;
		lottozahl[2] = 12;
		lottozahl[3] = 18;
		lottozahl[4] = 37;
		lottozahl[5] = 42;
		
		
		System.out.print("[  ");
		for(int i = 0; i < lottozahl.length; i++) {  //Array ausgeben
			System.out.print(lottozahl[i] + "  ");
		}		
		System.out.println("]");
		
		
		// Aufgabe b)
		
		boolean zahlVorhanden;
		int gefragteZahl;
		
		zahlVorhanden = false;
		gefragteZahl = 12;
		for(int i = 0; i < lottozahl.length; i++) {
		   if(lottozahl[i] == gefragteZahl) {
			   zahlVorhanden = true;
			   break;
		   }
		}
		if(zahlVorhanden) {
			   System.out.println("Die Glückszahl " + gefragteZahl +  " ist in der Ziehung enthalten.");			
		}
		else {
			   System.out.println("Die Glückszahl " + gefragteZahl +  " ist nicht in der Ziehung enthalten.");						
		}
		
		gefragteZahl = 13;
		zahlVorhanden = false;
		for(int i = 0; i < lottozahl.length; i++) {
		   if(lottozahl[i] == gefragteZahl) {
			   zahlVorhanden = true;
			   break;
		   }
		}
		if(zahlVorhanden) {
			System.out.println("Die Glückszahl " + gefragteZahl +  " ist in der Ziehung enthalten.");			
		}
		else {
			System.out.println("Die Glückszahl " + gefragteZahl +  " ist nicht in der Ziehung enthalten.");						
		}
	}
}
