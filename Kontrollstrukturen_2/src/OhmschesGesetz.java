import java.util.Scanner;

public class OhmschesGesetz {

	public static void main(String[] args) {
		char wert;
		double r, i, u;
		Scanner eingabe = new Scanner(System.in);

		System.out.println("Was moechten Sie berechnen (U, I, R)?: ");
		wert = eingabe.next().charAt(0);
		switch (wert)
		{
			case 'R':
			case 'r':
				System.out.println("Gib die Spannung in Volt ein: ");
				u = eingabe.nextDouble();
				System.out.println("Gib den Strom in Ampere ein: ");
				i = eingabe.nextDouble();
				r = u / i;
				System.out.printf("R = %.2f Ohm", r);
				break;
			case 'I':
			case 'i':
				System.out.println("Gib die Spannung in Volt ein: ");
				u = eingabe.nextDouble();
				System.out.println("Gib den Widerstand in Ohm ein: ");
				r = eingabe.nextDouble();
				i = u / r;
				System.out.printf("I = %.2f Ampere", i);
				break;
			case 'U':
			case 'u':
				System.out.println("Gib den Strom in Ampere ein: ");
				i = eingabe.nextDouble();
				System.out.println("Gib den Widerstand in Ohm ein: ");
				r = eingabe.nextDouble();
				u = i * r;
				System.out.printf("U = %.2f Volt", u);
				break;
			
			default:
				System.out.println("Inkorrekte Auswahl!");
				break;
		}

	}

}
